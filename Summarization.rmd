---
title: "Summarization_RMA"
author: Alexander Abi Saad, Aquilina Barhouche, Auriane Mahfouz, Charbel El Gemayel, Margueritta Abi Younes
date: "10/6/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Summarization of the background corrected, quantile normalized, log2 scaled expression values using the Median Polish method


### Median Polish

The Median Polish algorithm is used to fit the following model:

$log2(y_{ij}^{(n)}) = µ^{(n)} + θ_{j}^{(n)} + α_{i}^{(n)} + ε_{ij}^{(n)}$

Where:
<ul>
<li>n: nth probeset</li>
<li>y: background corrected, quantile normalized expression value at (i,j) in the probeset n</li>
<li>µ: overall effect of the probeset</li>
<li>θ<sub>j</sub>: column (array) effect</li>
<li>α<sub>i</sub>: row (probe) effect</li>
<li>ε<sub>ij</sub>: residual</li>
</ul>

The expression values are estimated by : $βˆ_{j}^{(n)} = µˆ^{(n)} + θˆ_{j}^{(n)}$

The algorithm proceeds as follows:

For each probeset <em>n</em> a matrix is formed, where probes are in rows and arrays are in columns.

This matrix is augmented with row and column effects to give a matrix of the form:

$$\begin{bmatrix}e_{11} & ... & e_{1N_{A}} & a_{1}\\
. & . & . & .\\
. & . & . & .\\
. & . & . & .\\
e_{I_{n}1} & ... & e_{I_{n}N_{A}} & a_{I_{n}}\\
b_{1} & ... & b_{N_{A}} & m
\end{bmatrix}$$

Initially, $e_{ij} = y_{ij}^{(n)}$ and $a_{i} = b_{j} = 0$

Then, each row is swept by taking its row median (excluding the last column), substracting it from each element in the row (except the last column), and adding it to the last column (i.e. to a<sub>1</sub>...m). A similar process is done for columns by taking the column median (excluding the last row of), substracting it from each value in the column (except the last row) and adding it to the last row (i.e. to b<sub>1</sub>...m).

We iterate the same process of row sweeps followed by column sweeps, until the changes become negligible. For our purposes, we did the process 4 times and found that it's giving good results.

At the end of the process $µˆ = m$ and $θˆ_{j} = b_{j}$. Therefore, we can estimate the final expression value for the gene/exon in the corresponding array <em>j</em> by $m + b_{j}$

Here, Median Polish returns the modified matrix
```{r}
medianPolish <- function (pSetMatrix)
{
  m <- pSetMatrix
  
  for(j in 1:4) {
    
    #row sweep
    cols = ncol(m)
    
    rmed = rowMedians(m[,-cols])
    
    #substract row median
    m <- cbind(sweep(m[,-cols], 1, rmed, "-"), m[,cols])
    #add to a
    m <- cbind(m[,-cols], sweep(matrix(m[,cols], ncol = 1), 1, rmed, "+"))
    
    
    #column sweep
    rows <- nrow(m)
    
    colmed <- colMedians(m[-rows,])
    
    #substract column median
    m <- rbind(sweep(m[-rows,], 2, colmed, "-"), m[rows,])
    #add to b
    m <- rbind(m[-rows,], sweep(matrix(m[rows,], nrow = 1), 2, colmed, "+"))
  }
  return(m)
}
```

Summarization proceeds as follows:

<ul>
<li>Map the individual probes to their corresponding probeset PROBEID using getProbeInfo() with the specified target</li>
<li>Loop over the PROBEIDs and for each PROBEID:
<ul><li>generate the corresponding probeset matrix</li>
<li>apply the Median Polish method on the generated matrix</li>
<li>put the summarized expression values in their corresponding row in the output matrix</li>
</ul>
<li>Return the final matrix</li>
</ul>

```{r}
summarization <- function (exp_data, target) {
  
  #log2 transformation of the background corrected, quantile normalized expression matrix
  log_expression <- log2(exp_data)
  
  
  #Check for summarization level, and generate probe info accordingly
  
  if (target == "core") { #gene-level
    #p_info: matrix containing individual probe indices in the 1st column, mapped to the corresponding transcript cluster PROBEID in the 2nd column
    p_info <- getProbeInfo(raw_data, target = "core")
  }
  if(target == "probeset"){ #exon-level
    #p_info: matrix containing individual probe indices in the 1st column, mapped to the corresponding probeset PROBEID in the 2nd column
    p_info <- getProbeInfo(raw_data, target = "probeset")
  }
  
  #create a sorted vector of the individual PROBEIDs, where each entry represents a probeset/transcript cluster
  probeids <- sort(unique(p_info$man_fsetid))
  
  #output matrix containing the summarized gene expression for each sample per probeset/transcript.
  #PROBEIDs in rows and samples in columns
  final_result = matrix(0, nrow = length(probeids), ncol = ncol(log_expression))
  
  #Set the rownames of the final output matrix to the corresponding PROBEID from the probeids vector
  rownames(final_result) = probeids
  #Set the colnames of the final output matrix to the corresponding sample name from the expression data matrix
  colnames(final_result) = colnames(exprs(raw_data))
  
  #Loop over the PROBEIDs, and for each one:
  #Get the list of all probes under this PROBEID
  #Group their expression values in a single matrix
  #Apply the Median Polish algorithm in order to get the corresponding gene/exon expression value for each sample
  #Put the summarized expression values in their corresponding row in final_result matrix
  
  for(i in 1:length(probeids)) {
    
    #Get the probeid at index i in probeids
    p_ID <- probeids[i]
    
    #Get the indices of the probes in log_expression belonging to the same PROBEID entry (i.e. belonging to the same probeset/transcript cluster)
    probes <- p_info[p_info$man_fsetid == p_ID, 1]
    
    
    #Get the expression values from the log_expression matrix of all the probes belonging to the same probeset/transcript cluster and group them in a matrix
    #pset <- result2[probes,]
    pset <- log_expression[probes,]

    
    #If the current probeset contains only 1 probe, then put it directly in its corresponding row in final_result, since the Median Polish algorithm wouldn't be applicable on it
    if(length(probes) == 1) {
        
        #convert the pset which would be a vector, to a matrix m
        m = as.matrix(pset, byrow = TRUE)
        #transpose matrix m in order to get the expression values along 1 row
        m = t(m)
        
        #put the values of m in the corresponding row in final_result matrix
        final_result[p_ID,] = m
    } else { 
  
    m = pset
    rm(pset)
    
    #Initialize the additional column that would be added to the probeset matrix.
    #This column represents the row effect
    a <- matrix (0, ncol =1, nrow = nrow(m))
    m <- cbind(m,a)
    
    #Initialize the additional row that would be added to the probeset matrix.
    #This would represent the column effect
    b <- matrix (0, nrow =1, ncol = ncol(m))
    m <- rbind(m, b)
    
    
    #MEDIAN POLISH
    m <- medianPolish(m)
    
    
    cols <- ncol(m)
    rows <- nrow(m)

    #In order to get the summarized expression value for each sample (column), add the overall effect to the corresponding column effect
    summarized_exp <- sweep(matrix(m[rows,-cols], nrow = 1), 1, m[rows,cols], "+")
    
    #Finally, put the summarized expression values for the current PROBEID in the corresponding row in final_result matrix
    final_result[p_ID,] <- summarized_exp[1,] 
    }
  }
  
  return(final_result)
}
```

Run the summarization function with the resulting matrix from the normalization step and with the specified target ("core" for gene level summarization, "probeset" for probeset level summarization). The function will return a matrix with the summarized expression values per sample.

```{r}

#Uncomment the following code to perform built in background correction and quantile normalization on raw_data

#library(preprocessCore)
#bg_corrected_data <- rma.background.correct(raw_data)
#norm_data <- normalize.quantiles(bg_corrected_data)

#Else, pass to the function the "result" matrix from QN_Final

summ_data <- summarization(result, "core")
```



## References

Bolstad, BM, Low-level Analysis of High-density Oligonucleotide Array Data: Background, Normalization and Summarization.
UC Berkeley, 2004 (Pages 67&69)